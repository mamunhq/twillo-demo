<?php
 //load the library
require ( dirname(__FILE__) . '/lib/twilio/Services/Twilio.php' );

    // if the caller pressed anything but 1 or 2 send them back
    if($_REQUEST['Digits'] != '1' AND $_REQUEST['Digits'] != '2') {
        header("Location: index.php");
        die;
    }
    else {
            // otherwise, if 1 was pressed just say "you pressed 1". If 2  just say "you pressed 1".
            $response = new Services_Twilio_Twiml();
            if ($_REQUEST['Digits'] == '1') { 
            $response->say('You pressed one');
            }
            if ($_REQUEST['Digits'] == '2') { 
            $response->say('You pressed two');
            }
            echo $response;
    }

<?php
/**
 * RogerLove
 *
 * @author Mamun Hoque
 * @copyright restricted
 * @version 0.0.1
 */


//load the library
require ( dirname(__FILE__) . '/lib/twilio/Services/Twilio.php' );


$response = new Services_Twilio_Twiml();
$response->say('Hello');
$gather = $response->gather(array(
	'numDigits' => 1,
	'action' => 'http://localhost/twilio-demo/handle-key.php'
	));

$gather->say('Hello Buddy, Press 1 to listen one. Press 1 to listen one. Press any other key to start over.');

print $response;
?>